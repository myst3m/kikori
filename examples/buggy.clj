;; *   Kikori
;; *
;; *   Copyright (c) Tsutomu Miyashita. All rights reserved.
;; *
;; *   The use and distribution terms for this software are covered by the
;; *   Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
;; *   which can be found in the file epl-v10.html at the root of this distribution.
;; *   By using this software in any fashion, you are agreeing to be bound by
;; * 	 the terms of this license.
;; *   You must not remove this notice, or any other, from this software.

(ns groovy-iot
  (:refer-clojure :exclude [read])
  (:require [kikori.core :as k]))

(k/refer-kikori)
(set-log-level! :warn)

(load-module "bme280")
(load-module "gps")
(load-module "irmagician")
(load-module "groovy-quatro")

(on-platform
 ;; IrMagician
 (+interface
  (config :UART {:path "/dev/ttyACM1" :baud-rate 9600 }) 
  (place :UART {:name "IRM"  :module :IrMagician :data-path "/tmp"}))
 
 ;; Groovy-IoT
 (+device :hidraw
          (config :system {:PCB "1.0.0" :power :5.0})
          (config :ADC {:vrm :VDD})
          (config :DAC {:vrm :VDD})

	  (bind :GP0 :GPIO)
          (bind :GP1 :ADC1)
          (bind :GP2 :ADC2)
          (bind :GP3 :DAC2)
          
          (place :GP0 {:name "GP0"})
          (place :GP1 {:name "ADC1"})
          (place :GP2 {:name "ADC2"})
          (place :GP3 {:name "DAC2"})




          ;; UART
          (config :UART {:path "/dev/ttyACM0" :baud-rate 115200}) 
	  (place :UART {:name "QUATRO" :module :GroovyQuatro})

          ;; I2C
          (config :I2C)
          (place :I2C {:addr 0x77 :prefix "BME" :module :BME280})))

;; Register systems
(boot!)

;; Web server
(srv/start-web-server :ip "0.0.0.0" :port 3000)

;; Console
(shell/repl)

