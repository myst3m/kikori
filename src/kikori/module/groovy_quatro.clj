;; *   Kikori
;; *
;; *   Copyright (c) Tsutomu Miyashita. All rights reserved.
;; *
;; *   The use and distribution terms for this software are covered by the
;; *   Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
;; *   which can be found in the file epl-v10.html at the root of this distribution.
;; *   By using this software in any fashion, you are agreeing to be bound by
;; * 	 the terms of this license.
;; *   You must not remove this notice, or any other, from this software.

(ns kikori.module.groovy-quatro
  (:refer-clojure :exclude [read write])
  (:require [kikori.core :refer :all]
            [kikori.module :refer (defmodule)])
  (:require [irmagician.core :as i]
            [irmagician.port :as p]))

(refer-kikori)


(defmodule GroovyQuatro
  (init [{:keys [] :as m}]
        (log/debug "GroovyQuatro init")
        m)
  (write [{:keys [uart string-data raw] :as sensor}]
        (log/debug "GroovyQuatro write" (or string-data raw))         
         (let [port (p/map->AsyncPort uart)]
           (if raw
             (p/write port (seq (map #(Long/parseLong
                                       (str/replace (str/trim %) #"^0[xX]" "") 16)
                                     (str/split raw #","))))
             (p/write port (str string-data "\n")))))

  (read [{:keys [uart] :as sensor}]
        (log/debug "GroovyQuatro read")
        (->> (p/read-bytes (p/map->AsyncPort uart))
             (drop-while #(not= 10 %) )
             (drop-last)))

  (close [sensor]
         (log/debug "GroovyQuatro close"))) 


